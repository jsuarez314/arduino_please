const int blue = 9;
const int red  = 10;
const int green = 11;
 
void setup() 
{
   Serial.begin(9600);
   pinMode(blue, OUTPUT);
   pinMode(red, OUTPUT);
   pinMode(green, OUTPUT);
}
 
void loop(){
   if (Serial.available()>0){
      char option = Serial.read();
      if (option == '1'){//blue
         digitalWrite(blue, HIGH);
         digitalWrite(red, LOW);
         digitalWrite(green, LOW);
      }
      if (option == '2'){//red
         digitalWrite(blue, LOW);
         digitalWrite(red, HIGH);
         digitalWrite(green, LOW);                 
      }
      if (option == '3'){//green
         digitalWrite(blue, LOW);
         digitalWrite(red, LOW);
         digitalWrite(green, HIGH);                 
      }
      if (option == '4'){//amarillo
         digitalWrite(blue, LOW);
         digitalWrite(red, HIGH);
         digitalWrite(green, HIGH);                 
      }
      if (option == '5'){//violeta
         digitalWrite(blue, HIGH);
         digitalWrite(red, HIGH);
         digitalWrite(green, LOW);                 
      }
      if (option == '6'){//cian
         digitalWrite(blue, HIGH);
         digitalWrite(red, LOW);
         digitalWrite(green, HIGH);                 
      }                        
      if (option == '7'){//blanco
         digitalWrite(blue, HIGH);
         digitalWrite(red, HIGH);
         digitalWrite(green, HIGH);                 
      }                       
      if (option == '8'){//shutdown
         digitalWrite(blue, LOW);
         digitalWrite(red, LOW);
         digitalWrite(green, LOW);                 
      }      
   }
}
