#!/usr/bin/python
# -*- coding: latin-1 -*-

import os, sys
import random as rand
import serial
from serial import Serial
import time
import numpy as np
import speech_recognition as sr
import pyttsx3

#Se inicia el motor de voz
engine = pyttsx3.init()
engine.setProperty('voice', "spanish")
#Se inicia el motor de reconocimiento de voz
r = sr.Recognizer()
r.energy_threshold = 4000  #Para lugares ruidosos

colores=["azul","rojo","verde","amarillo","magenta","cian","blanco","Azul","Rojo","Verde","Amarillo","Magenta","Cian","Blanco"]
crazy_ans=["Que grosero.","Ja ja ja, eres muy chistoso.","Parece que llueve.","Estas hablando en chino.","Me creeras que no te entiendo.","Querido amigo.","Que chistoso."]

arduino = serial.Serial('/dev/ttyACM0', 9600) 
time.sleep(2)
loop=True
print('Hola, me llamo Arduino')
engine.say('Hola, me llamo Arduino')
engine.runAndWait()
while(loop==True):
	with sr.Microphone() as source:
		print("Te escucho:")
		engine.say('Te escucho:')
		engine.runAndWait()
		audio = r.listen(source,timeout = 10)
		rec = r.recognize_google(audio,language="es-CO")
		order=""
		for i in rec:
			order=order+i 
		order=order.title()
		order=order.split()
	try:
		print("Acabas de decir \"" + rec +"\"")
		engine.say("Acabas de decir \"" + rec +"\"")
		engine.runAndWait()
		if len(order) == 3:
			a=(order[0]=="arduino" or order[0]=="Arduino")
			b=(order[1]=="dame" or order[1]=="Dame")
			#c=(order[3]=="por" or order[3]=="Por")
			#d=(order[4]=="favor" or order[4]=="Favor")
			color=order[2]
			#if (a and b and (c==False) and (d==False)):
			#	print("Que grosero, no me has pedido el favor.")

			if (a and b) and (color in colores):
				print("Por supuesto, iluminando de color " + color)
				engine.say("Por supuesto, iluminando de color " + color)
				engine.runAndWait()		
				index=colores.index(color)
				if(index==0 or index==7):
					print("Encendiendo azul")
					engine.say("Encendiendo azul")
					engine.runAndWait()		
					arduino.write(b'1')

				if(index==1 or index==8):
					print("Encendiendo rojo")
					engine.say("Encendiendo rojo")
					engine.runAndWait()		
					arduino.write(b'2')

				if(index==2 or index==9):
					print("Encendiendo verde")
					engine.say("Encendiendo verde")
					engine.runAndWait()		
					arduino.write(b'3')

				if(index==3 or index==10):
					print("Encendiendo amarillo")#Verde+ rojo
					engine.say("Encendiendo amarillo")
					engine.runAndWait()						
					arduino.write(b'4')

				if(index==4 or index==11):
					print("Encendiendo magenta")#azul+rojo
					engine.say("Encendiendo magenta")
					engine.runAndWait()		
					arduino.write(b'5')

				if(index==5 or index==12):
					print("Encendiendo cian")#verde + azul
					engine.say("Encendiendo Cian")
					engine.runAndWait()							
					arduino.write(b'6')

				if(index==6 or index==13):
					print("Encendiendo Blanco")#Blanco
					engine.say("Encendiendo blanco")
					engine.runAndWait()							
					arduino.write(b'7')

				engine.say("Quieres otro color?")
				engine.runAndWait()
				cont=input("Quieres otro color s/n?:")
				print(cont)
				if (cont=="s" or cont=="S"):
					engine.say("Continuemos")
					engine.runAndWait()
				else:
					loop=False
					print("Hasta la vista Baby!!")
					arduino.write(b'8')
					arduino.close()
					engine.say("Hasta la vista beibi!!")
					engine.runAndWait()		
			else:
				ans=rand.choice(crazy_ans)+" Intentalo otra vez!"
				print(ans)
				engine.say(ans)
				engine.runAndWait()
		else:
			print("Me hablas a mi? Intenta de nuevo.")
			engine.say("Me hablas a mi?, Intenta de nuevo.")
			engine.runAndWait()
	except sr.UnknownValueError:
	    print("Lo siento, no te entiendo!!")
	except sr.RequestError as e:
	    print("Could not request results; {0}".format(e))